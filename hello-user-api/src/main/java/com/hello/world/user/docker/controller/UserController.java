package com.hello.world.user.docker.controller;


import com.hello.world.user.docker.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/all")
    public List<User> allUsers() {
        List<User> users = new ArrayList<>();
        User nava = new User();
        nava.setEmail("nava.arul@gmail.com");
        nava.setName("Navaneetha Krishnan");

        User madhu = new User();
        madhu.setName("Madhusmita Pati");
        madhu.setEmail("madhu.ricky@gmail.com");
        users.add(nava);
        users.add(madhu);

        return users;
    }
}
